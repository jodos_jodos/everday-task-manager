require('dotenv').config();
require('express-async-errors');
const express=require('express');
const app = express();
const hbs=require('hbs');
const cookieParser=require('cookie-parser');
const signUpRoute=require('./routes/user');
const loginRoute=require('./routes/login');
const tokenHandler=require('./middleware/authentication');
const Task=require('./models/task')
// const taskRoute=require('./routes/tasks');
const path=require('path');
const partials=path.join(__dirname, './views/partials')
const editFile=path.join(__dirname, './views/partials/editTask.html')
//view engine
app.set('view engine', 'hbs');
//specify file to read
hbs.registerPartials(partials)
const taskRouter=require('./routes/tasks');
const connectDB=require('./db/connect');
const bodyParser=require('body-parser');
//will be used when validating logging in 

// test
const {requireAuth}=require('./middleware/authMiddlware')

const notFoundMiddleware=require('./middleware/not-found');
const errorHandleMiddleware=require('./middleware/error-handler')
//static and required files and measures 
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/public'));


//routes 


app.use('/',signUpRoute);

app.use('/login',loginRoute);
app.use('/login',requireAuth,taskRouter);
app.get('/login/:id', async (req, res) => {
   
  })
  
  app.patch('/login/:id', async (req, res) => {
 
  })

 
//errors handling

app.use(errorHandleMiddleware);
//port and listening

const port=process.env.PORT ||3000;
//connection and sart listening
const start=async()=>{
    try {
         await connectDB(process.env.MONGO_URI);
        app.listen(port,()=>{
            console.log(`app is running on port ${port}`);
        });
        
    } catch (err) {
       console.log(err);
       process.exit(1); 
    }
}
start();