const express=require('express');
const app = express();
const tasks=require('./routes/tasks');
const connectDB=require('./db/connect');
//custom error handling
const errorHandleMiddleware=require('./middleware/error-handler')
require('dotenv').config()
//middleware
app.use(express.json());
app.use(express.static('./public'))
//routes
app.get('')
app.use('/api/v1/tasks',tasks);
app.use(errorHandleMiddleware)
//connection to db
const port=process.env.PORT ||3000;
const start = async()=>{
    await connectDB(process.env.MONGO_URI);
    app.listen(port,()=>{
        console.log(`server is listening on  port ${port}`);
    });
    try {
    } catch (error) {
        console.log(error);
    }
}
start()