const Task=require('../models/task')

//get all tasks
const getAllTasks=async(req,res)=>{
   try{
    const tasks= await Task.find({});
    res.status(200).json({ tasks})
   }catch(err){
    res.status(404).json({message: err});
   }
}
//create one task simple way
const createTask=async(req,res)=>{
    try {
        const task=await Task.create(req.body);
        res.status(201).json({task})
    } catch (error) {
        res.status(500).json({msg: error.message})
    }

}
//using middleware

//get one task
const getTask=async(req,res)=>{
try {
    const {id:taskId}=req.params;
    const task=await Task.findOne({_id:taskId});
    if(!task){
        return res.status(404).json({msg:`not found  with${taskId}`});
    }
    res.status(200).json({task})
} catch (error) {
    res.status(500).json({msg: error})
}
}
//update one task
const updateTask=async(req,res)=>{
    try {
        
        const{id:taskId} = req.params;
    
        const task=await Task.findOneAndUpdate({_id:taskId},req.body,{new:true,runValidators:true})
        if(!task) {
            return res.status(404).json({msg:`no task found with id ${taskId} so no update happend`})
        }
        res.status(201).json({task})
    } catch (err) {
        res.status(500).json({msg:err})
    }

}
//delete one task
const deleteTask=async(req,res)=>{
    try {
        const{id:taskId}=req.params;
       const task=await Task.findOneAndDelete({_id:taskId});
       if(!task){

           return res.status(404).json({msg:`no id for task ${task}`})
        }
        
        res.status(200).json({msg:`user  ${task} has been deleted`})
                
    } catch (err) {
                res.status(500).json({msg:err})
     }
 }
module.exports ={
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    deleteTask
        
}
        