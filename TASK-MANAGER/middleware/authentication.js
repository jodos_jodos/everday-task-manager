const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { UnauthenticatedError } = require('../errors');

const auth = async (req, res, next) => {
  // check header
  const authHeader = req.headers.authorization;

  if (!authHeader || !authHeader.startsWith('Bearer')) {
    throw new UnauthenticatedError('Authentication invalid');
  }
  try {

    const token = authHeader.split(' ')[1];
    const payload = jwt.verify(token, process.env.JWT_SECRET);
// const user=await User.findById(payload.id).select('-password').then(user=>{req.user=user;}).cath(err => {res,send(err)})
// if(!user) throw new UnauthenticatedError('Authentication invalid');

    // attach user to job routes
    req.user = { UserId: payload.UserId, name: payload.name };

    next();
  } catch (err) {
   throw new UnauthenticatedError('authentication invalid')
  }
};

module.exports = auth;
