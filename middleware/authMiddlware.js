const jwt=require('jsonwebtoken');
const User=require('../models/user')
const requireAuth=(req,res,next)=>{
 const token=req.cookies.jwtLogin;
 //check if token exist
 if(token){
jwt.verify(token,process.env.JWT_SECRET,(err,decodedToken)=>{
    if(err){
        console.log(err);
        res.redirect('/loginPage')
    }
    else{
    //  console.log(decodedToken);
     next();
    }
});
 }else{
    res.redirect('/loginPage');
 }
}


module.exports={requireAuth};  