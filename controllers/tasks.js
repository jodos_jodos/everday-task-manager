const Task=require('../models/task');
const jwt=require('jsonwebtoken');
const StatusCodes=require('http-status-codes');

const getAllTasks=async(req,res)=>{
  try {
    const token =req.cookies.jwtLogin;
    if(token) {
        jwt.verify(token,process.env.JWT_SECRET,async(err,decodedToken)=>{
            if(err) {
                console.log(err);
            }else {
                const id=decodedToken.id;
              
                try {
                    const tasks=await Task.find({createdBy:id});
                    res.status(StatusCodes.OK).json({ tasks});
                } catch (err) {
                    console.log(err);
                }
            }
        })
    }
   
  } catch (err) {
    res.status().json({message: err})
  }

  
    
}
const getTask=async(req,res)=>{
    try {
        const task = await Task.findById(req.params.id)
        if (!task) {
          return res.status(404).send('Task not found')
        }
        res.json({ task })
      } catch (error) {
        console.error(error)
        res.status(500).send('Server Error')
      }
    // res.send('get single task')
}
const createTask=async(req,res)=>{
    try {
        const token=req.cookies.jwtLogin;
    if(token){
       jwt.verify(token,process.env.JWT_SECRET,async(err,decodedToken)=>{
     if(err){
        console.log(err);
           }
      else{
        const id=decodedToken.id;
        const name=req.body.name;
        const createdBy=id;
        try{
            const task=await Task.create({name,createdBy}); 
            res.status(StatusCodes.CREATED).json({task});
            // console.log(task);

        }catch(err){
           res.Status(StatusCodes.INTERNAL_SERVER_ERROR).json({msg:err})
        }
        
       }
     })
    }else{
        res.status(StatusCodes.BAD_REQUEST).json({msg:'not found'})
    }

    } catch (err) {
                        res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({msg:err.message});
    }
    
    // res.send('create task')
}
const updateTask=async(req,res)=>{
    const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'completed']
    const isValidUpdate = updates.every((update) =>
      allowedUpdates.includes(update)
    )
    if (!isValidUpdate) {
      return res.status(400).send('Invalid update')
    }
  
    try {
      const task = await Task.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true, runValidators: true }
      )
      if (!task) {
        return res.status(404).send('Task not found')
      }
      res.json({ task })
    } catch (error) {
      console.error(error)
      res.status(500).send('Server Error')
    }
}
const deleteTask=async(req,res)=>{
    try {
        const {id:taskId} = req.params;
        const task=await Task.findOneAndDelete({_id:taskId});
        if(!task) {
         return res.status(StatusCodes.NOT_FOUND).json({msg:`no id for task ${taskId}`})
        }
        res.status(StatusCodes.OK).json({msg:`task with id ${task} has been deleted`})
    } catch (err) {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({msg:err})
    }
  

}
module.exports={
    getAllTasks,
    getTask,
    createTask,
    updateTask,
    deleteTask
}