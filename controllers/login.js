const User=require('../models/user');
const StatusCodes=require('http-status-codes');
const {BadRequestError,NotFoundError}=require('../errors');
const authentication=require('../middleware/authentication')
const path=require('path');
const { log } = require('console');
const user = require('../models/user');
const { JsonWebTokenError } = require('jsonwebtoken');
const loginPage=path.join(__dirname,'../views/partials/login.hbs');
const taskManager=path.join(__dirname,'../public/task.html');
const jwt = require('jsonwebtoken');
//Things to  create tokenHandler
const maxAge=90*24*60*60
const createToken=(id)=>{
    return jwt.sign({id},process.env.JWT_SECRET,{expiresIn:maxAge});
}
const login=async(req,res) => {
   
    
    const requestPassword=req.body.password
    const requestEmail=req.body.email;
    

    const user=await User.findOne({email:requestEmail});
   
    
    if(!user) {
        return   res.status(StatusCodes.BAD_REQUEST).render(loginPage,{msg_type:'bad',msg:'user doesn\'t exist'});
    }
    
    const isPassword=await user.comparePassword(requestPassword);
    if(! isPassword){
     return   res.status(StatusCodes.UNAUTHORIZED).render(loginPage,{msg_type:'bad',msg:'incorrect password'})
      
    }
    const token=createToken(user._id);
            res.cookie('jwtLogin',token,{httpOnly:true,maxAge:maxAge *1000});
       
    res.sendFile(taskManager);

    
   

}
const logOut=async(req,res)=>{
    res.cookie('jwtLogin','',{maxAge:1});
    res.redirect('/');
}

module.exports ={login,logOut}
    

   
    
    


    
 
   
