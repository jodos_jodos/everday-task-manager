const bcrypt=require('bcryptjs');
const jwt=require('jsonwebtoken')
const mongoose=require('mongoose');
const {isEmail}=require('validator');
const registerSchema=new mongoose.Schema({
    userName:{
        type:String,
        required:[true,'please provide username'],
        trim:true,
    },
    email:{
        type:String,
        required:[true,'please provide email address'],
    //     match:[ /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    //   'Please provide a valid email',],
      validate:[isEmail,'please provide a valid email'],
      unique:true,
      lowercase:true
    },
    password:{
        type:String,
        required:[true,'please provide password'],
        minlength:[6,'password must be at least 6 characters']

    },
    confirmPassword:{
        type:String,
        required:[true,'please provide confirm password'],
        validator:{function(v){
            return v=== this.password
        },message:"confirm password must match password"}
        
        
    }
},{timestamps:true});
registerSchema.pre('save',async function(){
    const salt= await bcrypt.genSalt(10);
    this.password=await bcrypt.hash(this.password,salt);
    this.confirmPassword=await bcrypt.hash(this.confirmPassword,salt);
})
//crete token for password
registerSchema.methods.createJWT= function(){
    return jwt.sign({UserId:this._id,email:this.email},process.env.JWT_SECRET,{expiresIn:'30d'});
   
}
 //verify password
 registerSchema.methods.comparePassword = async function(candiatePassword){
    isMatch=await bcrypt.compare(candiatePassword,this.password);
    return isMatch;
}

module.exports =mongoose.model('Register',registerSchema);

