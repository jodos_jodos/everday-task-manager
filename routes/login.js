//this is login routes
const express=require('express');
const router=express.Router();
const {login,logOut}=require('../controllers/login');
// const { route } = require('./user');


router.route('/loginPage').get(login);
 router.route('/').post(login);
 router.route('/logout').get(logOut);
module.exports=router;